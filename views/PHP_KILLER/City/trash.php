<?php
require_once ("../../../vendor/autoload.php");

$obj = new \App\City\City();

$obj->setData($_GET);

$obj->trash();

\App\Utility\Utility::redirect('trashed.php');