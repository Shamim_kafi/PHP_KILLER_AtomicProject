<?php
require_once ("../../../vendor/autoload.php");

$path = $_SERVER ['HTTP_REFERER'];

$obj = new \App\profilePicture\ProfilePicture();

$obj->setData($_GET);
$obj->delete();

\App\Utility\Utility::redirect($path);