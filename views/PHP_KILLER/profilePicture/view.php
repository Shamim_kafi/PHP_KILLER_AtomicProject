<?php
require_once ("../../../vendor/autoload.php");

$obj = new \App\profilePicture\ProfilePicture();

$obj->setData($_GET);

$singleData = $obj->view();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <title>Document</title>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <h2 align="center">Single User - Profile Picture</h2>
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Profile Picture</th>
                    </tr>
                    <?php

                        echo "
                        
                         <tr>                      
                            <td>$singleData->id</td>
                            <td>$singleData->name</td>
                            <td><img src='images/$singleData->profile_picture' height='70' width='100' alt=''></td>
                         </tr>
                        
                        
                        ";

                    ?>
                </table>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>
</body>
</html>
