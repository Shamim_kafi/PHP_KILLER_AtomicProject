<?php
require_once ("../../../vendor/autoload.php");
$msg = \App\Message\Message::message();

$obj = new \App\Gender\Gender();

$allData = $obj->trashed();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/style/new/index.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <script src="../../../resources/bootstrap/js/jquery.js"></script>
    <link rel="stylesheet" href="../../../resources/style/new/reset.css">
    <title>Document</title>
    <style>
        body {
            background: radial-gradient(#FFF176,#F57F17);
            min-height: 100%;
            font: 400 14px 'Calibri','Arial';
            padding: 20px;
        }

        table {
            border-spacing: 1px;
            border-collapse: collapse;
            background: white;
            border-radius: 16px;
            overflow: hidden;
            max-width: 800px;
            width: 100%;
            margin: 0 auto;
            position: relative;
            padding: 30px;
            margin-bottom: 50px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div>
                    <div class="deck">
                        <div class="card one">
                            <div class="title"><a href="create.php">Create</a></div>
                            <div class="pic pic-one"></div>
                        </div>
                        <div class="card two">
                            <div class="title"><a href="index.php">Index</a></div>
                            <div class="pic pic-two"></div>
                        </div>
                        <div class="card three">
                            <div class="title"><a href="../button.php">Home</a></div>
                            <div class="pic pic-three"></div>
                        </div>
                        <div class="card four">
                            <div class="title"><a href="#">Trash List</a></div>
                            <div class="pic pic-four"></div>
                        </div>
                        <div class="card five">
                            <div class="title"><a href="../view.php">PHP KILLER</a></div>
                            <div class="pic pic-five"></div>
                        </div>
                    </div>
                </div>
                <div class="msg">
                    <?php
                    echo "<div id='message' style='text-align: center;color: red;font-size: large'>".$msg."</div>";
                    ?>
                </div>
                <h2 align="center" style="color: red;margin-top: -50px;font-size: 25px;border: 2px inset #f87f35; padding: 10px ">Trashed List- Profile Picture (<?php echo count($allData)?>)</h2>


                <form id="selectionForm" action=" " method="post">

                    <div style="margin: 20px">
                        <input type="submit" id="recoverMultipleButton" value="Recover Multiple" class="btn btn-lg " style="border-radius: 20px;margin: 5px 0 0 50px;background: radial-gradient(#fff176,#3a1dec);color: black;font-weight: bolder">
                        <input type="submit" id="deleteMultipleButton" value="Delete Multiple" class="btn btn-lg " style="border-radius: 20px;margin: 5px;background: radial-gradient(#FFF176,#f54729);color: black;font-weight: bolder">
                    </div>

                    <table>
                        <thead>
                            <tr>
                                <th style="text-align: center;font-size: large;padding-top: 15px;font-weight: bolder">Check Box <input type="checkbox" id='select_all' name='select_all' value='$record->id' ></th>
                                <th style="text-align: center;font-size: large;font-weight: bolder">Serial</th>
                                <th style="text-align: center;font-size: large;font-weight: bolder">ID</th>
                                <th style="text-align: center;font-size: large;font-weight: bolder">Name</th>
                                <th style="text-align: center;font-size: large;font-weight: bolder">Gender</th>
                                <th style="text-align: center;font-size: large;font-weight: bolder">Action</th>
                            </tr>
                        </thead>
                        <?php
                        $serial =1;
                        foreach ($allData as $record){

                            echo "
                        
                        <tbody>
                            <tr>
                                <td><input type='checkbox'  class='checkbox' name='multiple[]' value='$record->id'  style='margin-left: 30px;margin-top: 25px'></td>
                                <td style='text-align:center;font-size: large'>$serial</td>
                                <td style='text-align:center;font-size: large'>$record->id</td>
                                <td style='text-align:center;font-size: large'>$record->user_name</td>
                                <td style='text-align:center;font-size: large'>$record->gender</td>
                                <td>
                                    <a href='view.php?id=$record->id' class='btn btn-info' style='border-radius: 20px;margin-right:3px'>View</a>
                                    <a href='edit.php?id=$record->id' class='btn btn-success' style='border-radius: 20px; margin-right:3px'>Edit </a>
                                    <a href='recover.php?id=$record->id' class='btn btn-primary'  style='border-radius: 20px;margin-right:3px'>Recover</a>
                                    <a href='delete.php?id=$record->id' onclick=' return confirm_delete()' class='btn btn-danger'  style='border-radius: 20px;margin:3px'>Delete</a>
                                </td>
                             </tr>
                        </tbody>
                        
                        
                        ";
                            $serial++;
                        }
                        ?>
                        <tr>

                        </tr>
                    </table>
                </form>

            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>
















<script>

    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>



<script>

    function confirm_delete(){

        return confirm("Are You Sure?");

    }
</script>

<script>

    $('#recoverMultipleButton').click(function(){

        if(checkEmptySelection()){
            alert("Empty Selection! Please select some record first");
        }
        else{
            var r = confirm("Are you sure you want to Recover this selected record?");

            if(r){
                var selectionForm =   $('#selectionForm');
                selectionForm.attr("action","recover_multiple.php");
                selectionForm.submit();
            }
        }
    });
</script>

<script>

    $('#deleteMultipleButton').click(function(){

        if(checkEmptySelection()){
            alert("Empty Selection! Please select some record first");
        }
        else{
            var r = confirm("Are you sure you want to delete the selected record?");

            if(r){
                var selectionForm =   $('#selectionForm');
                selectionForm.attr("action","delete_multiple.php");
                selectionForm.submit();
            }
        }
    });
</script>

<script>

    function checkEmptySelection(){

        emptySelection =true;

        $('.checkbox').each(function(){
            if(this.checked)   emptySelection = false;
        });

        return emptySelection;
    }
</script>



<script>

    //select all checkboxes
    $("#select_all").change(function(){  //"select all" change
        var status = this.checked; // "select all" checked status
        $('.checkbox').each(function(){ //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
    });

    $('.checkbox').change(function(){ //".checkbox" change
//uncheck "select all", if one of the listed checkbox item is unchecked
        if(this.checked == false){ //if this item is unchecked
            $("#select_all")[0].checked = false; //change "select all" checked status to false
        }

//check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length ){
            $("#select_all")[0].checked = true; //change "select all" checked status to true
        }
    });



</script>



</body>
</html>
