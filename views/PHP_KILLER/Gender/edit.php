<?php
require_once ("../../../vendor/autoload.php");

$obj = new \App\Gender\Gender();
$obj->setData($_GET);
$singleData = $obj->view();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/style/gender.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <title>Gender</title>
</head>
<body>
<div class="container">
    <div id="content">
        <h1>Login Form</h1>
        <form action="update.php" method="post">
            <div class="wrapper">
                <input type="text" name="name" placeholder="User Name" value="<?php echo $singleData->user_name ?>">
            </div>
            <div class="gender">
                <span style="color: red">Gender : </span><input type="radio" name="Gender" value="Male" <?php if($singleData->gender == "Male") echo "checked = checked " ?>>Male
                <input type="radio" name="Gender" value="Female" <?php if($singleData->gender == "Female") echo "checked = checked" ?>>Female
            </div>
            <div>
                <input type="submit" value="update" >
            </div>
            <div>
                <input type="hidden" name="id" value="<?php echo $singleData->id ?>">
            </div>
        </form>
    </div>
</div>

</body>
</html>