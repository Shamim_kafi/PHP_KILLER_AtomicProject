<?php
require_once ("../../../vendor/autoload.php");

$obj = new \App\Hobies\Hobies();

$obj->setData($_GET);

$obj->trash();

\App\Utility\Utility::redirect('trashed.php');