<?php
require_once ("../../../vendor/autoload.php");

$objHobies = new \App\Hobies\Hobies();

$objHobies->setData($_POST);

$objHobies->store();

\App\Utility\Utility::redirect('create.php');