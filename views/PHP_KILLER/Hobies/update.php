<?php
require_once ("../../../vendor/autoload.php");

$objHobies = new \App\Hobies\Hobies();
$objHobies->setData($_POST);

$objHobies->update();

\App\Utility\Utility::redirect('index.php');