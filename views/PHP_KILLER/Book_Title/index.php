<?php
require_once ('../../../vendor/autoload.php');
use App\Message\Message;

$obj= new \App\BookTitle\BookTitle();

$allData = $obj->index();

$msg = Message::message();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <title>Update</title>
    <script src="../../../resources/bootstrap/js/jquery.js"></script>

    <script>


        jQuery(

            function($) {
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
            }
        )
    </script>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <?php
                echo "<div>  
                 <div id='message' style='color: red;padding: 10px;font-size: 18px;text-align: center'>  $msg </div>
                </div>";
                ?>
                <table class="table table-bordered table-striped">
                    <h2 style="text-align: center">Multiple Record Information - Book Title</h2>
                    <tr>
                        <th>Serial</th>
                        <th>ID</th>
                        <th>Book Name</th>
                        <th>Author Name</th>
                        <th>Action</th>
                    </tr>
                    <?php
                    $serial = 1;
                    foreach ($allData as $record){

                        echo "
        
                    <tr>
                       
                       <td>$serial</td>
                       <td>$record->id</td>
                       <td>$record->book_title</td>
                       <td>$record->author_name</td>
                       <td>
                           <a href='view.php?id=$record->id'><button class='btn btn-info'>View</button></a>
                           <a href='edit.php?id=$record->id'><button class='btn btn-success'>Edit</button></a>
                       </td>
                    
                    </tr>
                    
                    
                    ";
                        $serial++;
                    }
                    ?>
                </table>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>
</body>
</html>
