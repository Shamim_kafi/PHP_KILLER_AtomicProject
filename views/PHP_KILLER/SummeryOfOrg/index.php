<?php
require_once ("../../../vendor/autoload.php");

$obj = new \App\SummeryOfOrg\Summery();
$allData = $obj->index();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <script src="../../../resources/bootstrap/js/jquery.js"></script>
    <title>Document</title>
    <script>


        jQuery(

            function($) {
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
            }
        )
    </script>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="msg">
                    <?php
                    $msg = \App\Message\Message::message();

                    echo "<div style='text-align: center;color: red' id='message'>".$msg."</div>";

                    ?>
                </div>
                <h2 align="center">Multiple Summery - Comments</h2>
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>Serial</th>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Comments</th>
                        <th>Action</th>
                    </tr>
                    <?php
                    $serial = 1;
                    foreach ($allData as $record){

                        echo "
                        <tr>
                            <td>$serial</td>
                            <td>$record->id</td>
                            <td>$record->name</td>
                            <td>$record->summery</td>
                            <td>
                               <a href='view.php?id=$record->id'><button class='btn btn-info'>View</button></a>
                               <a href='edit.php?id=$record->id'><button class='btn btn-success'>Edit</button></a>
                            </td>
                            
                        </tr>
                        
                        ";
                        $serial++;
                    }
                    ?>
                </table>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>

</body>
</html>
