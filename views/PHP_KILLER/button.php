<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../resources/style/new/reset.css">
    <link rel="stylesheet" href="../../resources/style/button.css">
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <title>Document</title>

</head>
<body>
<div class="nav">
    <div class="row">
        <div class="col-md-3 col-lg-3">
            <ul class="pull-left">
                <li>
                    <a href="view.php" style="color: white;text-decoration: none;">Home</a>
                </li>
            </ul>
        </div>
        <div class="col-md-3 col-lg-6">
            <div class="logo">
                <h1>
                    <span style="border-radius: 50%;font-style: Aladin">P</span>
                    <span style="border-radius: 50%;font-style: Aladin">H</span>
                    <span style="border-radius: 50%;font-style: Aladin">P</span>
                    <span style="border-radius: 50%;font-style: Aladin">K</span>
                    <span style="border-radius: 50%;font-style: Aladin">I</span>
                    <span style="border-radius: 50%;font-style: Aladin">L</span>
                    <span style="border-radius: 50%;font-style: Aladin">L</span>
                    <span style="border-radius: 50%;font-style: Aladin">E</span>
                    <span style="border-radius: 50%;font-style: Aladin">R</span>
                </h1>
            </div>
        </div>
        <div class="col-md-3 col-lg-3">
            <ul class="pull-right">

            </ul>

        </div>
    </div>
</div>



<div class="container-fluid" style="margin-top:70px ">
    <h2>Atomic Project</h2>
    <div class="container-fluid" align="center">

        <div class="col-md-4">
            <a href="Book_Title/index.php"><button>Book Title</button></a>
        </div>
        <div class="col-md-4">
            <a href="Birthday/index.php"><button>Birthday</button></a>
        </div>
        <div class="col-md-4">
            <a href="City/index.php"><button>Country</button></a>
        </div>
    </div>

    <div class="container-fluid" align="center" style="margin-top: 50px">
        <div class="col-md-3">
            <a href="Gender/index.php"><button>Gender</button></a>
        </div>
        <div class="col-md-3">
            <a href="Hobies/index.php"><button>Hobbies</button></a>
        </div>
        <div class="col-md-3">
            <a href="SummeryOfOrg/index.php"><button>Summery</button></a>
        </div>
        <div class="col-md-3">
            <a href="profilePicture/index.php"><button>Profile Picture</button></a>
        </div>
    </div>
</div>



<div class="container-fluid navbar-fixed-bottom">
    <div class="col-md-4"></div>

    <div class="col-md-4" align="center">
        <h4>Designed By- PHP KILLER</h4>
    </div>

    <div class="col-md-4"></div>

</div>
</body>
</html>