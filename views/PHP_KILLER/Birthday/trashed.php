<?php
require_once ("../../../vendor/autoload.php");
use App\Message\Message;
$obj = new \App\Birthday\Birthday();
$allData = $obj->trashed();

$msg = Message::message();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
    <title>Document</title>
    <script src="../../../resources/bootstrap/js/jquery.js"></script>

    <script>
        jQuery(

            function($) {
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
                $('#message').fadeIn (550);
                $('#message').fadeOut (550);
            }
        );
        
        function confirm_delete() {
            return confirm("Are you sure?????????");
        }
    </script>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <?php
                echo "<div>  
                        <div id='message' style='color: red;padding: 10px;font-size: 18px;text-align: center'>  $msg </div>
                    </div>";
                ?>
                <table class="table-bordered table table-striped">
                    <h2 style="text-align: center">Trashed List Information - Birthday</h2>
                    <tr>
                        <th>Serial</th>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Date Of Birth</th>
                        <th>Action</th>
                    </tr>
                    <?php
                    $serial = 1;
                    foreach ($allData as $record){
                        echo "
                        
                        <tr>
                            <td>$serial</td>
                            <td>$record->id</td>
                            <td>$record->name</td>
                            <td>$record->dob</td>
                            <td>
                            <a href='view.php?id=$record->id'><button class='btn btn-info'>View</button></a>
                            <a href='edit.php?id=$record->id'><button class='btn btn-success'>Edit</button></a>
                            <a href='recover.php?id=$record->id'><button class='btn btn-primary'>Recover</button></a>
                            <a href='delete.php?id=$record->id' onclick='return confirm_delete()'><button class='btn btn-danger'>Delete</button></a>
                            </td>
                        </tr>
                        
                       
                        ";
                        $serial++;
                    }
                    ?>
                </table>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>

</body>
</html>
