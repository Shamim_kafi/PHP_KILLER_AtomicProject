
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title</title>
    <link rel="stylesheet" href="../../../resources/style/birthday.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <div id="content">
        <h1>BirthDay Date</h1>
        <form action="store.php" method="post">
            <div class="wrapper">
                <input type="text" name="name" placeholder="Enter Your Name" required>
            </div>
            <div class="wrapper">
                <input type="date" name="dob" required>
            </div>
            <div>
                <input type="submit" value="Submit">
            </div>
        </form>
    </div>
    <?php

    require_once("../../../vendor/autoload.php");

    use App\Message\Message;

    $msg = Message::message();

    echo " 
                 <div id='message' style='color: red;padding: 10px;font-size: 18px;text-align: center'>  $msg </div>
            ";

    ?>
</div>
<script src="../../../resources/bootstrap/js/jquery.js"></script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>

</body>
</html>