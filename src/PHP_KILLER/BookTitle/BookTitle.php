<?php
namespace App\BookTitle;



use PDO;
use App\Model\Database;
use App\Message\Message;

class BookTitle extends Database
{

    public $id;
    public $bookTitle;
    public $authorName;

    public function setData($postArray){

        if (array_key_exists('id',$postArray)){

            $this->id = $postArray['id'];
        }

        if (array_key_exists('bookTitle',$postArray)){

            $this->bookTitle = $postArray['bookTitle'];
        }
        if (array_key_exists('authorName',$postArray)){

            $this->authorName = $postArray['authorName'];
        }
    }//set Data

    public function store(){

        $bookName = $this->bookTitle;
        $authorName = $this->authorName;

        $dataArray = array($bookName,$authorName);

        $sqlQuery ="INSERT INTO `book_title` (`book_title`, `author_name`) VALUES (?,?);";
        $sth = $this->db->prepare($sqlQuery);

        $result = $sth->execute($dataArray);

        if($result){
            Message::message("Success! Data has been inserted Successfully!");
        }
        else{
            Message::message("Error! Data has not been inserted.");

        }
    }

    public function index(){

        $sqlQuery = "select * from book_title";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData =  $sth->fetchAll();

        return $allData;
    }

    public function view(){

        $sqlQuery = "select * from book_title WHERE id=".$this->id;
        $sth = $this->db->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $singleData =  $sth->fetch();

        return $singleData;
    }

    public function update(){

        $bookName = $this->bookTitle;
        $authorName = $this->authorName;

        $dataArray = array($bookName,$authorName);

        $sqlQuery ="UPDATE `book_title` SET `book_title` = ? , `author_name` = ? WHERE `id`=$this->id";
        $sth = $this->db->prepare($sqlQuery);

        $result = $sth->execute($dataArray);

        if($result){
            Message::message("Success! Data has been updated Successfully!");
        }
        else{
            Message::message("Error! Data has not been updated.");

        }
    }


}//End BookTitle Class
