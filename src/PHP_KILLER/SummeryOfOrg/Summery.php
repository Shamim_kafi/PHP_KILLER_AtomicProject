<?php
namespace App\SummeryOfOrg;

use PDO;
use App\Message\Message;
use App\Model\Database;

class Summery extends Database
{

    public $id;
    public $name;
    public $summery;

    public function setData($rcv){

        if (array_key_exists('id',$rcv)){
            $this->id =  $rcv ['id'];
        }
        if (array_key_exists('name',$rcv)){
            $this->name =  $rcv ['name'];
        }

        if (array_key_exists('summery',$rcv)){
            $this->summery =  $rcv ['summery'];
        }
    }//end set data

    public function store(){

        $name = $this->name;
        $summery =  $this->summery;
        $dataArray = array($name,$summery);

        $sql = "INSERT INTO `summery` (`name`, `summery`) VALUES (?,?);";
        $sth = $this->db->prepare($sql);
        $result = $sth->execute($dataArray);

        if ($result){

            Message::message("Successfully! Data has been inserted.. ");
        }else{

            Message::message("Data has not been inserted...");
        }
    }

    public function index(){

        $sqlQuery = "select * from summery";

        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData = $sth->fetchAll();
        return $allData;
    }
    public function view(){

        $sqlQuery = "select * from summery WHERE id= $this->id";

        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);

        $singleData = $sth->fetch();
        return $singleData;
    }

    public function update(){

        $name = $this->name;
        $summery = $this->summery;

        $dataArray = array($name,$summery);
        $sqlQuery = "UPDATE `summery` SET `name` = ?, `summery` = ? WHERE `id` = $this->id;";
        $sth = $this->db->prepare($sqlQuery);

        $result = $sth->execute($dataArray);
        if($result){
            Message::message("Successfully!! Data update...");
        }else {

            Message::message("Error!!!");
        }
    }


}//end Summery class