<?php
namespace App\Hobies;

use PDO;
use App\Message\Message;
use App\Model\Database;

class Hobies extends Database
{

    public $id;
    public $name;
    public $hobie;

    public function setData($rcv){

        if (array_key_exists('id',$rcv)){

            $this->id = $rcv['id'];
        }

        if (array_key_exists('name',$rcv)){

            $this->name = $rcv['name'];
        }
        if (array_key_exists('hobie',$rcv)){

            $hob = $rcv['hobie'];
            $this->hobie = implode(',',$hob);

        }
    }

    public function store(){

        $name = $this->name;
        $hobie = $this->hobie;
        $dataArray = array($name,$hobie);

        $sql = "INSERT INTO `hobies` (`name`, `hobies`) VALUES (?,?);";
        $sth = $this->db->prepare($sql);

        $result = $sth->execute($dataArray);

        if ($result){

            Message::message("Successfully!! Data Inserted....");
        }else{

            Message::message("Error!!Please right this error...");
        }

    }

    public function index(){

        $sqlQuery = "select * from hobies WHERE is_trashed = 'NO'";

        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData = $sth->fetchAll();
        return $allData;

    }

    public function view(){

        $sqlQuery = "select * from hobies WHERE id = $this->id";

        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);

        $singleData = $sth->fetch();
        return $singleData;

    }

    public function update(){

        $name = $this->name;
        $hobie = $this->hobie;
        $dataArray = array($name,$hobie);

        $sql = "UPDATE `hobies` SET `name` = ?, `hobies` = ? WHERE `id` = $this->id";
        $sth = $this->db->prepare($sql);

        $result = $sth->execute($dataArray);

        if ($result){

            Message::message("Successfully!! Data Updated....");
        }else{

            Message::message("Error!!Please right this error...");
        }

    }

    public function trash(){

        $sqlQuery = "UPDATE `hobies` SET is_trashed = NOW() WHERE `id` =$this->id";

        $result = $this->db->exec($sqlQuery);

        if ($result){

            Message::message("Successful!! Data has been trashed");
        }else{

            Message::message("Error!! ");
        }

    }

    public function trashed(){

        $sqlQuery = "select * from hobies WHERE is_trashed <> 'NO'";

        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData = $sth->fetchAll();

        return $allData;
    }

    public function recover(){

        $sqlQuery = "UPDATE `hobies` SET is_trashed = 'NO' WHERE `id` =$this->id";

        $result = $this->db->exec($sqlQuery);

        if ($result){

            Message::message("Successful!! Data has been recovered");
        }else{

            Message::message("Error!! ");
        }

    }

    public function delete(){

        $sqlQuery = "DELETE from hobies  WHERE `id` =$this->id";

        $result = $this->db->exec($sqlQuery);

        if ($result){

            Message::message("Successful!! Data has been deleted");
        }else{

            Message::message("Error!! ");
        }
    }



    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byUser']) && isset($requestArray['byHobbies']) )  $sql = "SELECT * FROM `hobies` WHERE `is_trashed` ='NO' AND (`name` LIKE '%".$requestArray['search']."%' OR `hobies` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byUser']) && !isset($requestArray['byHobbies']) ) $sql = "SELECT * FROM `hobies` WHERE `is_trashed` ='NO' AND `name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byUser']) && isset($requestArray['byHobbies']) )  $sql = "SELECT * FROM `hobies` WHERE `is_trashed` ='NO' AND `hobies` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->db->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()


    public function getAllKeywords(){
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->hobies);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->hobies);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords



    public function indexPaginator($page=1,$itemsPerPage=3){


        $start = (($page-1) * $itemsPerPage);

        if($start<0) $start = 0;


        $sql = "SELECT * from hobies  WHERE is_trashed = 'NO' LIMIT $start,$itemsPerPage";


        $STH = $this->db->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;


    }
}













