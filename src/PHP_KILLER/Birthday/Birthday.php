<?php
namespace App\Birthday;

use PDO;
use App\Message\Message;
use App\Model\Database;

class Birthday extends Database
{

    public $id;
    public $name;
    public $dob;

    public function setData($rcv){

        if(array_key_exists('id',$rcv)){
            $this->id = $rcv['id'];
        }
        if(array_key_exists('name',$rcv)){
            $this->name = $rcv['name'];
        }
        if(array_key_exists('dob',$rcv)){
            $this->dob = $rcv['dob'];
        }
    }

    public function store(){

        $name = $this->name;
        $birthday = $this->dob;

        $dataArray = array($name,$birthday);
        $sql = "INSERT INTO `birthday` (`name`, `dob`) VALUES (?,?);";

        $sth = $this->db->prepare($sql);
        $result = $sth->execute($dataArray);

        if ($result){

            Message::message("Successful!! Data has been inserted");
        }else{

            Message::message("Error!! Please give valid information");
        }
    }

    public function index(){

        $sqlQuery = "select * from birthday WHERE is_trashed = 'No'";

        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData = $sth->fetchAll();

        return $allData;
    }

    public function view(){

        $sqlQuery = "select * from birthday WHERE id=$this->id";

        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);

        $singleData = $sth->fetch();

        return $singleData;
    }

    public function update(){

        $name = $this->name;
        $birthday = $this->dob;

        $dataArray = array($name,$birthday);
        $sql = "UPDATE `birthday` SET `name` = ?, `dob` = ? WHERE `birthday`.`id` =$this->id";

        $sth = $this->db->prepare($sql);
        $result = $sth->execute($dataArray);

        if ($result){

            Message::message("Successful!! Data has been Updated");
        }else{

            Message::message("Error!! ");
        }
    }

    public function trash(){

        $sqlQuery = "UPDATE `birthday` SET is_trashed = NOW() WHERE `id` =$this->id";

        $result = $this->db->exec($sqlQuery);

        if ($result){

            Message::message("Successful!! Data has been trashed");
        }else{

            Message::message("Error!! ");
        }
    }

    public function trashed(){

        $sqlQuery = "select * from birthday WHERE is_trashed <> 'No'";

        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData = $sth->fetchAll();

        return $allData;
    }

    public function recover(){

        $sqlQuery = "UPDATE `birthday` SET is_trashed = 'No' WHERE `id` =$this->id";

        $result = $this->db->exec($sqlQuery);

        if ($result){

            Message::message("Successful!! Data has been recover");
        }else{

            Message::message("Error!! ");
        }
    }

    public function delete(){

        $sqlQuery = "delete from birthday WHERE `id` =$this->id";

        $result = $this->db->exec($sqlQuery);

        if ($result){

            Message::message("Successful!! Data has been deleted");
        }else{

            Message::message("Error!! ");
        }
    }


}